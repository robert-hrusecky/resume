OUTPUT := resume.pdf
INPUT := resume.tex
BUILD_DIR := build
OUTPUT_FULL := $(BUILD_DIR)/$(OUTPUT)

all: $(OUTPUT_FULL)

$(BUILD_DIR):
	@mkdir $(BUILD_DIR)

$(OUTPUT_FULL): $(BUILD_DIR) $(INPUT)
	@echo "===== Building PDF ====="
	@cd build && pdflatex ../$(INPUT)

show: $(OUTPUT_FULL)
	@echo "===== Opening in Evince ====="
	@evince $(OUTPUT_FULL) 2> /dev/null &

clean:
	rm -r $(BUILD_DIR)

.PHONY: show clean
